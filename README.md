Source for a Jupyter notebook docker containers with   
Pytorch and vrfb support for OpenAI Gym animations is found in

```
    ./src/jupyter-notebook
```

Note that in addition to the Jupyter notebook container we  
also have submodules for nginx and docker-gen to provide an   
Nginx https proxy and auto-configation of the Nginx proxy   
(via docker-gen). These are here mainly to keep all the components   
for the course in one place and simplify provisioning the whole   
service onto arbitrary VMs.

So you probably don't need to look at or worry about anything except

```
    ./src/jupyter-notebook
```

